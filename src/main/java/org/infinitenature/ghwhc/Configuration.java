package org.infinitenature.ghwhc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;

@org.springframework.context.annotation.Configuration
@ConfigurationProperties(prefix = "ghwhc")
public class Configuration {
  private List<String> projects = new ArrayList<>();
  private List<String> secrets = new ArrayList<>();
  private List<String> pushEventCommands = new ArrayList<>();

  public List<String> getProjects() {
    return projects;
  }

  public List<String> getSecrets() {
    return secrets;
  }

  public List<String> getPushEventCommands() {
    return pushEventCommands;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Configuration [projects=");
    builder.append(projects);
    builder.append(", secrets=");
    builder.append(secrets);
    builder.append(", pushEventCommands=");
    builder.append(pushEventCommands);
    builder.append("]");
    return builder.toString();
  }


}
