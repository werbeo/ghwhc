package org.infinitenature.ghwhc.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import org.infinitenature.ghwhc.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WebHookService {
  private static final Logger LOGGER = LoggerFactory.getLogger(WebHookService.class);
  @Autowired
  private Configuration config;

  public String processPushEvent(String project) {
    String command = config.getPushEventCommands().get(config.getProjects().indexOf(project));
    LOGGER.info("Call: {}", command);

    CompletableFuture.supplyAsync(() -> runCommand(command));

    return "done";
  }

  public Integer runCommand(String command) {
    try {
      Process process = new ProcessBuilder(command).start();
      try (InputStream is = process.getInputStream();
          Scanner scanner = new Scanner(is);
          Scanner s = scanner.useDelimiter("\\Z")) {

        String output = s.next();
        LOGGER.info("Output of {} is {}", command, output);

      }
    } catch (IOException e) {
      LOGGER.error("Failure excuting command", e);
    }
    return 1;
  }
}
