package org.infinitenature.ghwhc.rest;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import org.infinitenature.ghwhc.Configuration;
import org.infinitenature.ghwhc.GitHubUtility;
import org.infinitenature.ghwhc.service.WebHookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Path("/webhook/{project}")
@Component
public class GitHubWebHook {
  private static final Logger LOGGER = LoggerFactory.getLogger(GitHubWebHook.class);
  @Autowired
  private WebHookService webhookService;
  @Autowired
  private Configuration configuration;

  @POST
  public String webhook(@HeaderParam("x-hub-signature") String xHubSignature,
      @HeaderParam("X-GitHub-Event") String xGitHubEvent, @PathParam("project") String project,
      String payload) {
    LOGGER.info("x-hub-signature: {}", xHubSignature);
    LOGGER.info("X-GitHub-Event: {}", xGitHubEvent);
    LOGGER.info("Recived by post: {}", payload);
    LOGGER.info("project: {}", project);

    validateEvent(xGitHubEvent);
    validateProject(project);
    validateSignature(payload, xHubSignature,
        configuration.getSecrets().get(configuration.getProjects().indexOf(project)));

    return webhookService.processPushEvent(project);

  }

  private void validateSignature(String payload, String signature, String secret) {
    if (!GitHubUtility.verifySignature(payload, signature, secret)) {
      throw new NotAuthorizedException("Bad signature");
    }
    
  }

  private void validateProject(String project) {
    if (configuration.getProjects().indexOf(project) == -1) {
      throw new NotFoundException("The project " + project + " does not exists");
    }
  }

  private void validateEvent(String xGitHubEvent) {
    if (!"push".equals(xGitHubEvent)) {
      throw new IllegalArgumentException("Only github push events are supported.");
    }
  }
}
