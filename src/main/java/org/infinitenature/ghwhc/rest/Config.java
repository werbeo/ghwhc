package org.infinitenature.ghwhc.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.infinitenature.ghwhc.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/config")
public class Config {
  private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);
  @Autowired
  private Configuration config;
  @GET
  public String dumpConfig() {
    LOGGER.info("Config: {}", config);
    return "done, look into the logs.";
  }
}
