package org.infinitenature.ghwhc.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/v1/api")
public class JaxrsApplication extends Application {

}
